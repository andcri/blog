<!-- wp:jetpack/markdown {"source":"In this small guide we will see how to get a blinky program up and running on your nrf52 dk using rust embedded.\n\nPrerequisites:\n\n- Rust installed (link)\n- Openocd installed (link)\n\nWe now clone this repo:\n\n```\ngit clone https://github.com/nrf-rs/nrf-hal.git\n```\nThis will give us access to all the configuration that we need to build our binary file in the correct way using rust.\n\nWe can now go to the following folder\n\n```\ncd boards/nRF52-DK\n```\n\nand build our example\n\n```\ncargo build \u002d\u002dtarget thumbv7em-none-eabihf \u002d\u002dexample blinky\n```\n\nNOTE: You might need to add this target in your rustc, to do that simply\n```\nrustc add target thumbv7em-none-eabihf\n```\nThe binary will be saved in the target folder in the root of the repo.\n\nWe can now initiate a connection via openocd, in order to do that we need to specify to it the correct cfg file for the nrf52, here is mine\n```\nsource [find interface/jlink.cfg]\ntransport select swd\nsource [find target/nrf52.cfg]\n```\nWe run openocd from wherever you have installed it( link above to set it up if you didn't yet)\nEs.\n```\n./src/openocd -s tcl -f nrf52.cfg\n```\n\n**Debug with gdb**\n\nOnce openocd has connected to our board we can gdb into the executable and connect to the port 3333 that openocd has opened for us.\n```\ngdb-multiarch -q ../../target/thumbv7em-none-eabihf/debug/examples/blinky\n\ntarget remote :3333\n```\n\nWe can now load our program\n```\nload\n```\n\nAnd continue to see the blinky program in action\n```\ncontinue\n```\n\n**Flash with telnet**\n\nTo permanently flash the program to our board, we are connecting via telnet to the port 4444.\nType in the terminal\n```\ntelnet localhost 4444\n```\nonce connected you can now see the info about your board by typing\n```\ntargets\n```\nAs you will see the state of your device is 'running', we want to stop it before doing any erasing and flashing, in order to do that we type this\n```\nreset halt\n```\nWe can now erase the board\n```\nnrf51 mass_erase\n```\nflash the program permanently and restart the board\n \n```\nprogram ../../target/thumbv7em-none-eabihf/debug/examples/blinky verify reset\n.\n.\n.\nreset run\n```\n\nGreat! We were able to flash a program written in rust on our nRF52-DK. I hope it was useful.\n"} -->
<div class="wp-block-jetpack-markdown"><p>In this small guide we will see how to get a blinky program up and running on your nrf52 dk using rust embedded.</p>
<p>Prerequisites:</p>
<ul>
<li>Rust installed</li>
<li>Openocd installed (https://www.hashdefineelectronics.com/compiling-and-installing-openocd-with-cmcsis-dap-support/)</li>
</ul>
<p>We now clone this repo:</p>
<pre><code>git clone https://github.com/nrf-rs/nrf-hal.git
</code></pre>
<p>This will give us access to all the configuration that we need to build our binary file in the correct way using rust.</p>
<p>We can now go to the following folder</p>
<pre><code>cd boards/nRF52-DK
</code></pre>
<p>and build our example</p>
<pre><code>cargo build --target thumbv7em-none-eabihf --example blinky
</code></pre>
<p>NOTE: You might need to add this target in your rustc, to do that simply</p>
<pre><code>rustc add target thumbv7em-none-eabihf
</code></pre>
<p>The binary will be saved in the target folder in the root of the repo.</p>
<p>We can now initiate a connection via openocd, in order to do that we need to specify to it the correct cfg file for the nrf52, here is mine</p>
<pre><code>source [find interface/jlink.cfg]
transport select swd
source [find target/nrf52.cfg]
</code></pre>
<p>We run openocd from wherever you have installed it( link above to set it up if you didn't yet)
Es.</p>
<pre><code>./src/openocd -s tcl -f nrf52.cfg
</code></pre>
<p><strong>Debug with gdb</strong></p>
<p>Once openocd has connected to our board we can gdb into the executable and connect to the port 3333 that openocd has opened for us.</p>
<pre><code>gdb-multiarch -q ../../target/thumbv7em-none-eabihf/debug/examples/blinky

target remote :3333
</code></pre>
<p>We can now load our program</p>
<pre><code>load
</code></pre>
<p>And continue to see the blinky program in action</p>
<pre><code>continue
</code></pre>
<p><strong>Flash with telnet</strong></p>
<p>To permanently flash the program to our board, we are connecting via telnet to the port 4444.
Type in the terminal</p>
<pre><code>telnet localhost 4444
</code></pre>
<p>once connected you can now see the info about your board by typing</p>
<pre><code>targets
</code></pre>
<p>As you will see the state of your device is 'running', we want to stop it before doing any erasing and flashing, in order to do that we type this</p>
<pre><code>reset halt
</code></pre>
<p>We can now erase the board</p>
<pre><code>nrf51 mass_erase
</code></pre>
<p>flash the program permanently and restart the board</p>
<pre><code>program ../../target/thumbv7em-none-eabihf/debug/examples/blinky verify reset
.
.
.
reset run
</code></pre>
<p>Great! We were able to flash a program written in rust on our nRF52-DK. I hope it was useful.</p>
</div>
<!-- /wp:jetpack/markdown -->